import java.util.Calendar;
import java.util.GregorianCalendar;


/** 
 * Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 
 * En esta versión sólo se ha aplicado un diseño OO básico.
 *  Se pueden detectar varios defectos y antipatrones de diseño:
 *  	- Obsesión por los tipos primitivos.
 *      - Exceso de métodos estáticos.
 *  	- Clase acaparadora, que delega poca responsabilidad. 
 *  	- Clase demasiado grande.
 * @since: prototipo1.0
 * @source: JVPrincipal.java 
 * @version: 1.6 - 4.1.19
 * @author: jpr NRE4004010
 */

public class JVPrincipal {
	
	private static final int MAX_SESIONES = 10;
	private static final int MAX_USUARIOS = 10;
	
	//almacen de datos del programa
	private static Usuario[] datosUsuarios = new Usuario[MAX_USUARIOS];
	private static SesionUsuario[] datosSesiones = new SesionUsuario[MAX_SESIONES];
	private static int sesionesRegistradas = 0;

	public static void main(String[] args) {	
		
//		cargarUsuariosPrueba();
//		mostrarTodosDatosUsuarios();
//		
//		//crea un objeto tipo SeseionUsuario y lanza el metodo de inicio de sesion
//		SesionUsuario sesion = new SesionUsuario();
//		System.out.println(sesion.toString());
//		if (sesion.inicioSesionCorrecto()) {
//			registrarSesion(sesion);
			Simulacion sim = new Simulacion();
			sim.lanzarDemo();
//			
//		}
//		for (SesionUsuario s : datosSesiones) {
//			System.out.println(s);
//		}
//	
	}
	/**
	 * método que busca el nif del usuario en el almacén de datos 
	 * @param UsuarioNif
	 * @return devuelve a dicho usuario si lo encuentra o usuario null si no
	 */
	public static Usuario buscarUsuario(String UsuarioNif) {
		
		Usuario UsuarioEncontrado = null; 
		
		for (int i = 0; i < datosUsuarios.length; i++ ) {
			if (datosUsuarios[i].getNif().equals(UsuarioNif)) {
				UsuarioEncontrado = datosUsuarios[i];
			} 			
		}
		return UsuarioEncontrado;
	}
	private static void registrarSesion(SesionUsuario sesion) {	
		
		datosSesiones[sesionesRegistradas] = sesion;
		sesionesRegistradas++;
		System.out.println("Sesion registrada correctamente en posición "+sesionesRegistradas+"/10");
		
	}
	/**
	 * método que crea tantas copias diferentes de usuarios como MAX_USUARIOS
	 */
	private static void cargarUsuariosPrueba() {
		
		for (int i = 0 ; i< MAX_USUARIOS ; i++ ) {
			Usuario jugador = new Usuario();
			
			String nombre = "jugador"+i;
			String dni = "0000000"+ i + "T";
			String password = "Miau#"+i;
			String dir = "calle viento, nº " + i;			
			Calendar fechaNacimiento = new GregorianCalendar();
			fechaNacimiento.set(1980+i, 1+i, 4+i);
			
			
			jugador.setFechaNacimiento(fechaNacimiento);
			jugador.setNombre(nombre);
			jugador.setClaveAcceso(password);
			jugador.setNif(dni);
			jugador.setDomicilio(dir);
			datosUsuarios[i] = jugador;	
		}
	}
	private static void mostrarTodosDatosUsuarios() {
		for (int i = 0; i < MAX_USUARIOS; i++) {
			System.out.println(datosUsuarios[i].toString());
		}
	}
	
	
} //class
