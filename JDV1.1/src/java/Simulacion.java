import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Calendar;
import java.util.GregorianCalendar;

/** 
 * Proyecto: Juego de la vida.
 * Organiza aspectos de gestión de la simulación según el modelo1.
 * En esta versión sólo se ha aplicado un diseño OO básico.
 * Se pueden detectar varios defectos y antipatrones de diseño:
 *  	- Obsesión por los tipos primitivos.
 *  	- Clase demasiado grande.
 *  	- Clase acaparadora, que delega poca responsabilidad.  
 * @since: prototipo1.0
 * @source: Simulacion.java 
 * @version: 1.6 5.1.19
 * @author: jpr 4004010
 */

public class Simulacion {	

	private Usuario usr;	
	private Calendar fecha;	
	private byte[][] mundo;	
	
	private static final int MUNDO_SIZE = 9;
	private static final int CICLOS_SIMULACION = 20;
	
	//constructor tipo1
	public Simulacion(Usuario usr, Calendar fecha, byte[][] mundo) {
		setUsr(usr);
		setFecha(fecha);
		setMundo(mundo);
	}
	
	//constructor por defecto
	public Simulacion() {
		this.usr = new Usuario();
		this.fecha =  new GregorianCalendar();
		this.mundo = new byte[MUNDO_SIZE][MUNDO_SIZE];
		
	}
	
	public Usuario getUsr() {
		return usr;
	}
	public void setUsr(Usuario usr) {
		assertNotNull(usr);
		this.usr = usr;
	}
	public Calendar getFecha() {
		return fecha;
	}
	public void setFecha(Calendar fecha) {
		assertNotNull(fecha);
		this.fecha = fecha;
	}
	public byte[][] getMundo() {
		return mundo;
	}
	public void setMundo(byte[][] mundo) {
		assertNotNull(mundo);
		this.mundo = mundo;
	}
		
	public Simulacion(Simulacion simulacion) {
		this.usr =  new Usuario(simulacion.usr);
		this.fecha =  (Calendar) simulacion.fecha.clone(); //obtiene un objeto replica independiente  // se castea (Calendar) porque es un Object el clon
		
		//matriz que representa al mundo
		this.mundo = new byte[simulacion.mundo.length] [simulacion.mundo.length];	
						  	
		for (int i=0; i < simulacion.mundo.length; i++) { //itera fila a fila la matriz
						
			System.arraycopy(simulacion.mundo[i], 0, this.mundo[i], 0, 							
							simulacion.mundo[i].length); //arraycopy permite copiar datos dentro de una matriz y entre matrices
		}
	
	}

	public void lanzarDemo(){
		
		cargarMundoDemo();
		
		for (int gen = 0; gen < CICLOS_SIMULACION; gen++) {
			mostrarMundo(mundo);
			actualizarMundo();
		}
				
	}	
	 private void actualizarMundo() {
		 //copia del mundo inicial
	    	byte[][] nuevoMundo = new byte[MUNDO_SIZE][MUNDO_SIZE];
	        
	    	
	            
	        	for (int i = 0; i < MUNDO_SIZE; i++) {
	                for (int j = 0; j < MUNDO_SIZE; j++) {
	                    nuevoMundo[i][j] = mundo[i][j];
	                }
	            }
	            
	            for (int i = 0; i < MUNDO_SIZE; i++) {
	                for (int j = 0; j < MUNDO_SIZE; j++) {
	                    if (mundo[i][j] == 1 && !(hallarVecinos(mundo, i, j) == 2 || hallarVecinos(mundo, i, j) == 3)) {
	                    	nuevoMundo[i][j] = 0; //muere
	                    }
	                    else if    (mundo[i][j] == 0 && hallarVecinos(mundo, i, j) == 3) {
	                    	nuevoMundo[i][j] = 1; 
	                    }
	                }
	            } 
	            mundo = nuevoMundo;  
		
	}
	    
	 private int hallarVecinos(byte[][] board, int a, int b) {
	        int count = 0;
	        int[][] surrounding = {{a - 1, b - 1},
	                               {a - 1, b    },
	                               {a - 1, b + 1},
	                               {a    , b - 1},
	                               {a    , b + 1},
	                               {a + 1, b - 1},
	                               {a + 1, b    },
	                               {a + 1, b + 1}};
	        for (int[] i: surrounding) {
	            try {
	                if (board[i[0]][i[1]] == 1) {
	                    count++;
	                }
	            }
	            catch (ArrayIndexOutOfBoundsException e) {}
	        }
	        return count;
	    }

	private void mostrarMundo(byte[][] mundo) {
		 for (int i = 0; i < MUNDO_SIZE; i++) {
	            for (int j = 0; j < MUNDO_SIZE; j++) {
	                if (mundo[i][j]==1) {
	                    System.out.print(" ■ ");
	                }
	                else {
	                    System.out.print(" □ ");
	                }
	            }
	            System.out.println();
	        }
	        System.out.println();		
	}

	private  void cargarMundoDemo() {
    			mundo = new byte [][]{
    			{0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0, 0, 0},
                {0, 0, 1, 1, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
	}

	@Override
	 public String toString() {
		 return String.format(	"%-16s %s\n"
				 				+ "%-16s %s\n",
				 				"Usuario:", usr.toString(),
				 				"Fecha simulacion:", new GregorianCalendar()
				 				);
	 }
	
	
} //class
