import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de SesionUsuario segÃºn el modelo1
 *  En esta versiÃ³n sÃ³lo se ha aplicado un diseÃ±o OO bÃ¡sico.
 *  Se pueden detectar varios defectos y antipatrones de diseÃ±o:
 *  	- Clase perezosa, que asume poca responsabilidad.
 *  @since: prototipo1.0
 *  @source: SesionUsuario.java 
 *  @version: 1.0 - 2018/11/21 
 *  @author: ajp
 */

public class SesionUsuario {
	
	private Usuario usr;
	private Calendar fechaInicioSesion;
	private final static int MAX_INTENTOS_PERMITIDOS = 3;
	
	
	//contrsuctor desde atributos
	public SesionUsuario(Usuario usuario, Calendar fechaSesion) {
		setUsr(usuario);
		setFecha(fechaSesion);
	}
	
	// constructor por defecto
	public SesionUsuario() {
		this.usr = new Usuario();
		this.fechaInicioSesion = new GregorianCalendar();
	}
	
	// constructor copia desde clase
	public SesionUsuario(SesionUsuario sesionUsuario) {
		this.usr = new Usuario(sesionUsuario.usr);
		this.fechaInicioSesion = new GregorianCalendar(	sesionUsuario.fechaInicioSesion.get(Calendar.YEAR), 
											sesionUsuario.fechaInicioSesion.get(Calendar.MONTH),
											sesionUsuario.fechaInicioSesion.get(Calendar.DATE));
		
	}
	
	public Usuario getUsr() {
		return usr;
	}
	public void setUsr(Usuario usuario) {
		assertNotNull(usuario);
		this.usr = usuario;
	}	
	
	public Calendar getFecha() {
		return fechaInicioSesion;
	}
	public void setFecha(Calendar fecha) {
		assertNotNull(fecha);		
			this.fechaInicioSesion = fecha;		
	}	
	/**
	 * este metodo pregunta al usuario que introduzca los credenciales tantas veces como
	 * intentos mÃ¡ximos se permiten y devuel que sean correctos	 * 
	 * @returns true si encuentra al usuario en el almacen y la clave coincide con la almacenada
	 */
	public boolean inicioSesionCorrecto() {

		 /* este metodo pregunta al usuario que introduzca los credenciales tantas veces como
		 * intentos mÃ¡ximos se permiten y devuel que sean correctos
		 */
		Scanner teclado = new Scanner(System.in);
		int intentosInicioSesion = 0;
		String nifIntroducido = "";
		String passIntroducido = "";		
		boolean credencialesCorrectos = false;
		Usuario usrEncontrado = null;
		
		/*bucle que pregunta los datos al usuario de acceso hasta que los pone correctos 
		 * o llega al limite de intentos*/
		while (intentosInicioSesion < MAX_INTENTOS_PERMITIDOS && credencialesCorrectos == false) {
			
			System.out.println("Introduzca sus credenciales\nNIF: ");
			nifIntroducido = teclado.nextLine();			
			System.out.println("Contraseña: ");			
			passIntroducido = teclado.nextLine();	
			usrEncontrado = JVPrincipal.buscarUsuario(nifIntroducido);			
			
			if (usrEncontrado != null && usrEncontrado.getClaveAcceso().equals(passIntroducido)) {
				//System.out.println("credenciales OK");
				credencialesCorrectos = true;				
			}
			else {
				
				System.out.println("Error: El usuario y/o contraseña incorrectos o no existen."
						+ "\nQueda(n) "+ (MAX_INTENTOS_PERMITIDOS-intentosInicioSesion-1) +" intento(s)");
				intentosInicioSesion++;				
			}
		}
		return credencialesCorrectos;
		
	}
	@Override
	public String toString() {		
		
		return String.format(	"%-16s %s\n"
								+ "%-16s %s\n",
								"Usuario:", usr.toString(),
								"Fecha de inicio de sesion:", this.fechaInicioSesion.get(Calendar.YEAR) 
								+ "." 
								+ this.fechaInicioSesion.get(Calendar.MONTH) 
								+ "." 
								+ this.fechaInicioSesion.get(Calendar.DATE)			
							);		
	}
	
} // class
